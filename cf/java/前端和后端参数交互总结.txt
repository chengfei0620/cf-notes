一.前端页面向后端传递数据.

1普通类型参数:  参数名与处理器方法形参名保持一致
前端:  http://localhost/requestParam1?name=tom&age=18
后台:  public String requestParam1(String name , String age){}

2.简单POJO类型参数:参数名称与POJO类属性名保持一致
前端:http://localhost/requestParam2?name=tom&age=18
后台:
    实体类:public class User{
	private String name;
	private String age;
	}
    控制层:public String requestParam2(User user){}

3.复杂POJO参数类型:
    1.当POJO中出现集合,保存简单数据,使用多个相同名称的参数为其赋值
前端:http://localhost/requestParam2?name=tom&age=18

mvc01天


二.后端向前端传递数据


