集合
	集合是存储对象数据的容器。
	集合相比数组的优势：
		集合可以存储任意类型的对象数据，而数组只能存储同种类型的数据。
		集合的长度是可变的，数组的长度是固定的。
	====================================单列结合==========================================
	---| Collection  单列集合的跟接口 
	------| List  如果是实现了List接口的集合类，具备的特点： 有序，可重复。
	---------| ArrayList   ArrayList底层是维护了一个Object数组实现的,特点: 查询速度快，增删慢。
	---------| LinkedList  LinkedList底层是使用了链表数据结构实现的, 特点: 查询速度慢，增删快。
	---------| Vector      Vector底层是维护了一个Object的数组实现的，但是Vector是线程安全的，操作效率低。
	------| Set   如果是实现了Set接口的集合类，具备特点： 无序，不可重复。
	---------| HashSet  底层是使用了哈希表来支持的，特点：存取速度快. 
	---------| TreeSet  如果元素具备自然顺序特性，那么就按照元素自然顺序的特性进行排序存储。
	Collection集合的常用方法：
		boolean add(E e) 向集合中添加元素
		boolean addAll(Collection<E> c) 将指定集合中的所有元素都添加到此集合中 

		boolean remove(Object o) 移除集合中的指定元素 
		boolean removeAll(Collection<E> c) 移除包含在此集合中的另外一个集合的所有元素。 
		boolean retainAll(Collection<E> c) 保留包含在此集合中的另外一个集合的所有元素。
		void clear() 清空集合

		boolean contains(Object o) 判断集合中是否包含某元素。 
		boolean containsAll(Collection<?> c) 判断集合中是否包含另一个集合的所有元素。 
		boolean equals(Object o) 比较两个集合是否相等。 
		boolean isEmpty() 判断集合是否为空。 
		
		int size() 查看集合元素个数。 
		Iterator<E> iterator() 迭代。 
		Object[] toArray() 以Object数组的方式返回包含此集合中所有元素。 
	List接口
		特有方法：
			添加
				add(int index, E element) 在指定位置插入指定元素
				addAll(int index, Collection<? extends E> c)  在指定位置插入指定集合的所有元素
			获取：
				get(int index) 根据索引值查找集合元素
				indexOf(Object o)  查找指定元素在集合中第一次出现的位置
				lastIndexOf(Object o) 查找指定元素在集合中最后一次出现的位置
				subList(int fromIndex, int toIndex) 根据索引值截取集合，返回一个新的集合
			修改：
				set(int index, E element) 根据索引修改元素
			迭代
				listIterator() 获取一个listIterator迭代器
		List接口中特有的方法具备的特点： 操作的方法都存在索引值。	
		只有List接口下面的集合类才具备索引值。其他接口下面的集合类都没有索引值。
	ArrayList
		ArrayList 特有的方法：
			ensureCapacity(int minCapacity) 调整初始底层数组大小
			trimToSize()  调整底层的数组为当前实际数据大小
		ArrayList底层是维护了一个Object数组实现的,特点:查询速度快，增删慢。使用无参构造函数时，Object数组默认的容量是10，当长度不够时，自动增长0.5倍。
	LinkedList
		LinkedList底层是使用了链表数据结构实现的, 特点: 查询速度慢，增删快。
		Linkedlist特有的方法：
			addFirst(E e) 将元素添加到集合首位置
			addLast(E e)  将元素添加到集合末位置
			getFirst()    获取集合首位置元素
			getLast() 	  获取集合末位置元素
			removeFirst() 移除集合首位置元素
			removeLast()  移除集合末位置元素
			push()  将元素插入到集合的开头处
			pop()   移除并返回集合中的第一个元素
			offer() 将元素添加到集合末位置
			poll()  删除集合的首元素
			descendingIterator() 获取一个逆序迭代器
	Vector
		底层也是维护了一个Object的数组实现的，实现与ArrayList是一样的，但是Vector是线程安全的，操作效率低。
		ArrayLsit与Vector的区别：
		相同点：ArrayList与Vector底层都是使用了Object数组实现的。
		不同点： 
			1. ArrayList是线程不同步的，操作效率高;Vector是线程同步的，操作效率低。
			2. 在底层数组长度不满足要求时，ArrayList增长为原来的1.5倍，Vector增长为原来的2倍。
			3. ArrayList是JDK1.2出现，Vector是jdk1.0的时候出现的。
	Set接口
	
	HashSet
		HashSet的实现原理：
			往HashSet添加元素的时候，HashSet会先调用元素的hashCode方法得到元素的哈希值,然后通过元素的哈希值经过移位等运算，就可以算出该元素在哈希表中的存储位置。
			情况1： 如果算出元素存储的位置目前没有任何元素存储，那么该元素可以直接存储到该位置上。
			情况2： 如果算出该元素的存储位置目前已经存在有其他的元素了，那么会调用该元素的equals方法与该位置的元素再比较一次,如果equals返回的是true，那么该元素与这个位置上的元素就视为重复元素，不允许添加，如果equals方法返回的是false，那么该元素运行添加。
	TreeSet
		TreeSet底层是使用红黑树(二叉树)数据结构实现的
		TreeSet使用注意细节:
			1. 往TreeSet添加元素的时候，如果元素本身具备了自然顺序的特性，那么就按照元素自然顺序的特性进行排序存储。
			2. 往TreeSet添加元素的时候，如果元素本身不具备自然顺序的特性，那么该元素所属的类必须要实现Comparable接口，把元素的比较规则定义在compareTo(T o)方法上。 
			3. 如果比较元素的时候，compareTo方法返回的是0，那么该元素就被视为重复元素，不允许添加.(注意：TreeSet与HashCode、equals方法是没有任何关系。)
			4. 往TreeSet添加元素的时候, 如果元素本身不具备自然顺序的特性，而元素所属的类也没有实现Comparable接口，那么必须要在创建TreeSet的时候传入一个比较器。
				定义比较器的格式 ：
 				方式一：让被比较的类去实现comparable接口，重写其compareTo()方法
					class 被比较的类 implements Comparable{
						int compareTo(Object o){
							
						}
					}
				方式二：自定义比较器类实现comparator接口，并重写其compare()方法,然后在创建TreeSet时将其通过treeSet的构造方法传入
					class  自定义比较器类  implements Comparator{
						int compare(Object o1,Object o2){
							
						}
					}
					new TreeSet(new 自定义比较器类())
			5. 往TreeSet添加元素的时候，如果元素本身不具备自然顺序的特性，而元素所属的类已经实现了Comparable接口， 在创建TreeSet对象的时候也传入了比较器那么是以比较器的比较规则优先使用。
			6. TreeSet是可以对字符串进行排序的， 因为字符串已经实现了Comparable接口。
				比较规则：
					情况一：对应位置有不同的字符出现，就比较的就是对应位置不同的字符。
					情况二：对应位置上的字符都一样，比较的就是字符串的长度。
	迭代器
		作用：就是用于抓取集合中的元素。
		Iterator的公共方法：
			hasNext()   判断是否有元素可遍历。如果有元素可以遍历，返回true，否则返回false。
			next()    先取出当前指针指向的元素，然后指针向下移动一个单位。
			remove()  移除迭代器最后一次返回的元素。
		ListIterator特有方法：
			hasPrevious()  判断是否存在上一个元素。
			previous()    当前指针先向上移动一个单位，然后再取出当前指针指向的元素。
			add(E e)   把当前有元素插入到当前指针指向的位置上。
			set(E e)   替换迭代器最后一次返回的元素。
		注意细节：
			在迭代器迭代元素的过程(这个过程指迭代器从创建到使用结束这段时间)中，不允许使用集合对象改变集合中的元素个数，如果需要添加或者删除只能使用迭代器的方法进行操作。
			如果使用过了集合对象改变集合中元素个数那么就会出现ConcurrentModificationException异常。					
					
	===========================================双列集合=================================================
	双列集合体系					
		---| Map 这是双列集合的根接口。实现了此接口的集合具备的特点：存储的数据都是以键值对的形式存在的，并且键是不可重复的。
		------| HashMap 底层是基于哈希表实现的。特点：存取快。
		------| TreeMap 底层是基于红黑树（二叉树）数据结构实现的，特点：会对元素的键进行排序存储。
		------| Hashtable 底层是基于哈希表实现的，为线程安全的，操作效率低。
	Map接口的方法
		V put(K key,V value)  向集合中添加键值对，如果之前没有存在该键，那么返回的是null，如果之前就已经存在该键了，那么就返回该键之前对应的值。    
		void putAll(Map<? extends K,? extends V> m)  将一个集合中的元素添加到另一个集合中 
	
		V remove(K key)  根据键删除集合中的一条数据，返回该键对应的值
		void clear()  清空集合
		
		V get(K key) 根据键获取对应的值
		int size() 获取集合中的键值对个数
		
		boolean containsKey(K key) 判断集合是否包含指定的键
		bollean containsValue(V value) 判断集合是否包含指定的值
		boolean isEmpty() 判断集合是否为空
		
		Set<K k> keySet() 返回一个包含所有集合中键的set视图
		Collection<V v> values() 返回一个包含所有集合中值的Collection视图 
		Set<Map.Entry<K,V>> entrySet() 返回一个元素为Map.Entry<String, String>的Set集合
		下面是三种遍历当时的应用范例：
			1、map集合中遍历方式一： 使用keySet方法进行遍历       缺点： keySet方法只是返回了所有的键，没有值。 
				Set<String> keys = map.keySet();
				Iterator<String> it = keys.iterator();
				while(it.hasNext()){
					String key = it.next();
					System.out.println("键："+ key+" 值："+ map.get(key));
				}
			2、map集合的遍历方式二： 使用values方法进行 遍历。    缺点： values方法只能返回所有 的值，没有键。
				Collection<String>  c = map.values(); 
				Iterator<String> it = c.iterator();
				while(it.hasNext()){
					System.out.println("值："+ it.next());
				}
			3、map集合的遍历方式三： entrySet方法遍历。
				Set<Map.Entry<String,String>>  entrys = map.entrySet(); 
				Iterator<Map.Entry<String,String>> it = entrys.iterator();
				while(it.hasNext()){
					Map.Entry<String,String> entry = it.next();
					System.out.println("键："+ entry.getKey()+" 值："+ entry.getValue());
				}
	HashMap类
		HashMap的存储原理：
			往HashMap添加元素的时候，首先会调用键的hashCode方法得到元素的哈希码值，然后经过运算就可以算出该元素在哈希表中的存储位置。 
			情况1：如果算出的位置目前没有任何元素存储，那么该元素可以直接添加到哈希表中。
			情况2：如果算出的位置目前已经存在其他的元素，那么还会调用该元素的equals方法与这个位置上的元素进行比较，如果equals方法返回 的是false，那么该元素允许被存储，如果equals方法返回的是true，那么该元素被视为重复元素，不允存储。
	TreeMap类
		TreeMap也是基于红黑树（二叉树）数据结构实现的，特点：会对元素的键进行排序存储。
		要注意的事项：
			1.  往TreeMap添加元素的时候，如果元素的键具备自然顺序，那么就会按照键的自然顺序特性进行排序存储。
			2.  往TreeMap添加元素的时候，如果元素的键不具备自然顺序特性，那么键所属的类必须要实现Comparable接口，把键的比较规则定义在CompareTo方法上。
			3.  往TreeMap添加元素的时候，如果元素的键不具备自然顺序特性，而且键所属的类也没有实现Comparable接口，那么就必须在创建TreeMap对象的时候传入比较器。
	注意：集合中线程安全的类有vector，stack，hashtable，enumeration，除此之外均是非线程安全的类与接口
	集合工具类：
		Collections是一个操作集合对象的工具类，Collection是单列集合的跟接口。
		常用方法：
			void sort(List l,Comparator c) 对list集合进行排序，可以根据需要传入比较器。
			int binarySearch(List l,K key,Comparator c)  二分法查找指定键值的元素，返回索引值
			T max(Collection c,Comparator co) 查找最大元素
			T min(Collection c,Comparator co) 查找最小元素
			reserve(List l) 翻转集合元素
			下面的方法用于需要线程安全时：
			List synchronizedList(List l)
			Map synchronized(Map m)
			Set synchronized(Set s)	

Properties(配置文件类)
	Properties类表示了一个持久的属性集，它是属于Map体系的类：主要用于生产配置文件与读取配置文件的信息。
	主要方法：
		Object setProperty(String key, String value)  设置Property属性
		void store(OutputStream os,String d)  通过输出流输出配置信息到文件 
		void load(InputStream inStream) 通过输入流读取文件信息 
	创建配置文件步骤：
		1、创建Propertity
			Properties properties = new Properties();
		2、添加属性
			properties.setProperty("狗娃", "123");
		3、使用Properties生产配置文件。
			//第一个参数是一个输出流对象，第二参数是使用一个字符串描述这个配置文件的信息。
			properties.store(new FileOutputStream("F:\\persons.properties"), "haha"); //中文默认使用的是iso8859-1码表进行编码存储
			properties.store(new FileWriter("F:\\persons.properties"), "hehe");
	读取配置文件步骤：
		1、创建Properties对象
			Properties properties = new Properties();
		2、加载配置文件信息到Properties中
			properties.load(new FileReader("F:\\persons.properties"));
		3、遍历出文件信息
			Set<Entry<Object, Object>> entrys = properties.entrySet();
			for(Entry<Object, Object> entry  : entrys){
				System.out.println("键："+ entry.getKey() +" 值："+ entry.getValue());
			}
	Properties要注意的细节：
		1. 如果配置文件的信息一旦使用了中文，那么在使用store方法生成配置文件的时候只能使用字符流解决，如果使用字节流生成配置文件的话，默认使用的是iso8859-1码表进行编码存储，这时候会出现乱码。
		2. 如果Properties中的内容发生了变化，一定要重新使用Properties生成配置文件，否则配置文件信息不会发生变化。
	

			
泛型					
	泛型是jdk1.5使用的新特性。
	泛型的好处：
		1. 将运行时的异常提前至了编译时。
		2. 避免了无谓的强制类型转换 。
  	泛型注意细节： 
		泛型只能为引用类型，不能为基本类型。
		泛型没有多态的概念，左右两边的数据类型必须要一致，或者只是写一边的泛型类型。
		泛型只在编译过程中生效，编译后的字节码中不存在泛型，这称作泛型擦除。
	自定义泛型：
		自定义泛型就是一个数据类型的占位符或者是一个数据类型的变量。
		1、方法上自定义泛型：
			修饰符	<声明自定义的泛型>返回值类型	函数名(使用自定义泛型 ...){
			
			}
			在泛型中不能使用基本数据类型，如果需要使用基本数据类型，那么就使用基本数据类型对应的包装类型。
				byte----> Byte
				short---> Short 
				int----> Integer
				long----> Long 
				double ----> Double 
				float -----> Float
				boolean----->Boolean
				char------->Character 
			方法泛型注意的事项：
				1. 在方法上自定义泛型，这个自定义泛型的具体数据类型是在调用该方法的时候传入实参时确定具体的数据类型的。
				2. 自定义泛型只要符合标识符的命名规则即可, 但是自定义泛型我们一般都习惯使用一个大写字母表示。
		2、类上自定义泛型
			泛型类的定义格式：
				class 类名<声明自定义泛型>{
				
				}
			注意的事项：
				1. 在类上自定义泛型的具体数据类型是在使用该类创建对象时候确定的。
				2. 如果一个类在类上已经声明了自定义泛型，如果使用该类创建对象的时候没有指定泛型的具体数据类型，那么默认为Object类型
				3. 在类上自定义泛型不能作用于静态的方法，如果静态的方法需要使用自定义泛型，那么需要在方法上自己声明使用。
		3、接口上自定义泛型
			泛型接口的定义格式: 
				interface 接口名<声明自定义泛型>{
				
				}
			注意的事项：
				1. 接口上自定义的泛型的具体数据类型是在实现一个接口的时候指定的。
				2. 在接口上自定义的泛型如果在实现接口的时候没有指定具体的数据类型，那么默认为Object类型。
				3、如果实现一个接口的时候，依旧不明确目前要操作的数据类型，要等待创建接口实现类对象的时候才能指定泛型的具体数据类型。那就要延长接口自定义泛型的具体数据类型，格式如下：  
					public class Demo<T> implements Dao<T>{
						
					}
	泛型的上下限:
		泛型中通配符：?
			? super 已知类 :    只能存储已知类或者是已知类的父类元素。  泛型的下限
 			? extends 已知类 ： 只能存储已知类或者是已知类的子类元素。  泛型的上限

			
