减少GC开销的措施
　	根据上述GC的机制,程序的运行会直接影响系统环境的变化,从而影响GC的触发。若不针对GC的特点进行设计和编码,就会出现内存驻留等一系列负面影响。
	为了避免这些影响,基本的原则就是尽可能地减少垃圾和减少GC过程中的开销。具体措施包括以下几个方面:
　　(1)不要显式调用System.gc()
　　	此函数建议JVM进行主GC,虽然只是建议而非一定,但很多情况下它会触发主GC,从而增加主GC的频率,也即增加了间歇性停顿的次数。
　　(2)尽量减少临时对象的使用
	　　临时对象在跳出函数调用后,会成为垃圾,少用临时变量就相当于减少了垃圾的产生,从而延长了出现上述第二个触发条件出现的时间,减少了主GC的机会。
　　(3)对象不用时最好显式置为Null
	　　一般而言,为Null的对象都会被作为垃圾处理,所以将不用的对象显式地设为Null,有利于GC收集器判定垃圾,从而提高了GC的效率。
　　(4)尽量使用StringBuffer,而不用String来累加字符串
	　　由于String是固定长的字符串对象,累加String对象时,并非在一个String对象中扩增,而是重新创建新的String对象，这样就会产生大量的中间垃圾对象。
　　(5)能用基本类型如Int,Long,就不用Integer,Long对象
	　　基本类型变量占用的内存资源比相应对象占用的少得多,如果没有必要,最好使用基本变量。
　　(6)尽量少用静态对象变量
	　　静态变量属于全局变量,不会被GC回收,它们会一直占用内存。
　　(7)分散对象创建或删除的时间
	　　集中在短时间内大量创建新对象,特别是大对象,会导致突然需要大量内存,JVM在面临这种情况时,只能进行主GC,以回收内存或整合内存碎片,从而增加主GC的频率。
		集中删除对象,道理也是一样的。它使得突然出现了大量的垃圾对象,空闲空间必然减少,从而大大增加了下一次创建新对象时强制主GC的机会。