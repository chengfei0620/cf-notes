Object类：
	Object类是所有类的终极父类。 任何一个类都继承了Object类。
	Object类常用的方法：
		toString()：返回该对象的字符串表示。 
		equals(Object obj)：用于比较两个对象的内存地址，判断两个对象是否为同一个对象。
		hashCode()：返回该对象的哈希码值(大家可以把哈希码就理解成是对象的内存地址)
			java中的规范：一般我们重写了一个类的equals方法，我们都会重写它的hashCode方法。
		notify() notifyAll() getClass() finalize()	
			
String 字符串类:
	String的创建方式
		方式一：String s1 = "hello";
			这种方式创建String时，jvm会先在方法区的常量池中寻找目标字符串，如果发现，立即将目标字符串的内存地址返回，如果未发现目标，会立即在方法区的常量池中创建对象，并将其内存地址返回;
		方式二：String s2 = new String("hello");
			这种方式创建String时，jvm会先在方法区的常量池中寻找目标字符串，如果发现就不再创建，如未发现，立即创建，然后会再去堆内存中创建一个对象，并将方法区中的目标对象拷贝到对内存中，并将堆内存中的目标对象的内存地址返回
	字符串比较方式
		Str1==Str2  这样比较的是两个字符串对象的内存地址
		Str1.equals(str2)  这样比较的是两个字符串的值(equals用于比较两个对象时默认比较两个对象的内存地址，但是String类重写了这个方法)
	String 的构造方法：
		String()  创建一个空内容 的字符串对象。
		String(byte[] bytes)  使用一个字节数组构建一个字符串对象
		String(byte[] bytes, int offset, int length) 
			bytes :  要解码的数组
			offset： 指定从数组中那个索引值开始解码。
			length：　要解码多个元素。
		String(char[] value)  使用一个字符数组构建一个字符串。	
		String(char[] value, int offset, int count)  使用一个字符数组构建一个字符串， 指定开始的索引值，与使用字符个数。
		String(int[] codePoints,int offset,int count)
		String(String original) 
	String的常用方法：
		获取方法
			int length();  //获取字符串的长度
			char charAt(int i);  //获取指定索引位置的字符
			int indexOf(String str);//查找指定字符串首次索引位置，如果不存在，返回-1
			int lastIndexOf(String str);//查找指定字符串最后一次出现的索引位置，如果不存在，返回-1
			int	hashCode() 返回字符串的哈希值，哈希码默认情况下表示的是内存地址，但是String类已经重写了Object的hashCode方法了。如果两个字符串的内容一致，那么返回的hashCode码肯定也会一致的。 
		判断方法
			Boolean startWith(Strinbg str) 是否以指定字符串结尾
			boolean endsWith(String str) 是否以指定字符结束
			boolean isEmpty()是否长度为0 如：“” null V1.6
			boolean contains(CharSequences) 是否包含指定序列 应用：搜索
			boolean equals(Object anObject) 是否相等
			boolean equalsIgnoreCase(String anotherString) 忽略大小写是否相等
		转换方法
			char[] toCharArray() 将字符串转换为字符数组
			byte[] getbytes()	将字符串转换为字节数组
				字节数组与字符数组、字符串他们三者之间是可以互相转换。
			String replace(String oldChar, String newChar) 替换
		其他方法
			String[] split(String regex) 切割
			String substring(int beginIndex)   指定开始的索引值截取子串
			String substring(int beginIndex, int endIndex)指定开始与结束的索引值截取子串【包括头，不包括尾】
			String toUpperCase() 转大写
			String toLowerCase() 转小写
			String trim() 去除字符串首尾的空格
		字符串特点：字符串是常量;它们的值在创建之后不能更改. 其内容一旦发生了变化，那么马上会创建一个新的对象。

字符串缓冲类
	StringBuffer：如果需要频繁修改字符串的内容，建议使用字符串缓冲类。
	StringBuffer 其实就是一个存储字符的容器，底层的实现是一个char[],其默认容量为16，拓展时会自动增长1倍。
	常用方法:
	增加
		append(boolean b)    可以添加任意类型的数据到容器中
		insert(int offset, boolean b)  指定插入的索引值，插入对应的内容。 
	删除
		delete(int start, int end)  根据指定的开始与结束的索引值删除对应的内容。
		deleteCharAt(int index)   根据指定的索引值删除一个字符。
	修改
		replace(int start, int end, String str) 根据指定的开始与结束索引值替代成指定的内容。
		reverse()   翻转字符串缓冲类的内容。  
        setCharAt(int index, char ch)  把指定索引值的字符替换指定的字符。 
        String substring(int start, int end)  根据指定的索引值截取子串。
		ensureCapacity(int minimumCapacity)  指定StringBuffer内部的字符数组长度的。
	查看
		indexOf(String str, int fromIndex) 查找指定的字符串第一次出现的索引值,并且指定开始查找的位置。
		lastIndexOf(String str) 查找指定的字符串最后一次出现的索引值,并且指定开始查找的位置。
		capacity() 查看当前字符数组的长度。 
		length() 查看存储字符的个数
		charAt(int index) 根据指定的索引值查找字符
		toString()  把字符串缓冲类的内容转成字符串返回。
	StringBuffer 与 StringBuilder的相同处与不同处：	
		相同点：
			1. 两个类都是字符串缓冲类。
			2. 两个类的方法都是一致的。
		不同点：
			1. StringBuffer是线程安全的,操作效率低,StringBuilder是线程非安全的,操作效率高。
			2. StringBuffer是jdk1.0出现的，StringBuilder 是jdk1.5的时候出现的。
	推荐使用： StringBuilder，因为操作效率高。

	
	
System类
	这是一个系统类，无构造方法，所有方法均为静态方法。常用的方法有：
		arraycopy(Object src, int srcPos, Object dest, int destPos, int length)根据指定条件拷贝数组
			src - 源数组。
			srcPos - 源数组中的起始位置。
			dest - 目标数组。
			destPos - 目标数据中的起始位置。
			length - 要复制的数组元素的数量。
		currentTimeMillis() 获取当前系统时间(由1970年到当前的时间毫秒数)
		exit(int status) 退出jvm,  status表示退出状态，0正常退出，非0表示异常退出。
		gc()    建议jvm赶快启动垃圾回收期回收垃圾。
			每个垃圾对象被回收时都会调用本身的finalize()方法
		getenv(String name)  根据环境变量的名字获取环境变量。
		getProperties() 获取当前系统属性，放回到Properties中
		getProperty(key)  根据key获取当前属性
		
		
		
RunTime   
	该类主要代表了应用程序运行的环境。
		getRuntime() 这是一个单例设计模式模式的类，使用getRuntime()返回当前应用程序的运行环境对象。
		exec(String command)  根据指定的路径执行对应的可执行文件,返回启动程序的进程Process。
		freeMemory()   返回 Java 虚拟机中的空闲内存量。。 以字节为单位
		maxMemory()    返回 Java 虚拟机试图使用的最大内存量。
		totalMemory()  返回 Java 虚拟机中的内存总量
		


日期类
	Date              时间类
	Calendar		  日历类
	SimpleDateFormat  日期格式化类
		String format()  可以将日期转化为指定格式的字符串
		Date parse()     可以将指定的字符串转化为对应日期
		例子：SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		   字母   日期或时间元素 
			y  		年    
			M  		年中的月份   
			w       年中的周数 
			W       月份中的周数
			D    	年中的天数  Number  189  
			d  		月份中的天数  Number  10  
			F  		月份中的星期  Number  2  
			E  		星期中的天数  Text  Tuesday; Tue  
			a  		Am/pm 标记  Text  PM  
			H  		一天中的小时数（0-23）  Number  0  
			k 		一天中的小时数（1-24）  Number  24  
			K 		am/pm 中的小时数（0-11）  Number  0  
			h 		am/pm 中的小时数（1-12）  Number  12  
			m  		小时中的分钟数  Number  30  
			s  		分钟中的秒数  Number  55  


		
Math类
	主要是提供了很多的数学公式。
    abs(double a)  获取绝对值
	ceil(double a)  向上取整
	floor(double a)  向下取整
	round(float a)   四舍五入
	random()   产生一个随机数. 大于等于 0.0 且小于 1.0 的伪随机 double 值


Random类



线程
	进程:正在执行的程序称作为一个进程。进程负责了内存空间的划分。
	线程:线程在一个进程中负责了代码的执行，就是进程中一个执行路径，
	多线程:在一个进程中有多个线程同时在执行不同的任务。
	一个java应用程序至少有几个线程？
		至少有两个线程， 一个是主线程负责main方法代码的执行，一个是垃圾回收器线程,负责了回收垃圾。
	多线程的好处：
		1. 解决了一个进程能同时执行多个任务的问题。
		2. 提高了资源的利用率。
	多线程的弊端：
		1. 增加cpu的负担。
		2. 降低了一个进程中线程的执行概率。
		3. 引发了线程安全问题。
		4. 出现了死锁现象。
	创建线程的方式：
		方式一：
			1.自定义一个类继承Thread类。
			2.重写Thread类的run方法 , 把自定义线程的任务代码写在run方法中
			  重写run方法的目的是：  
				每个线程都有自己的任务代码，jvm创建的主线程的任务代码就是main方法中的所有代码, 自定义线程的任务代码就写在run方法中，自定义线程负责了run方法中代码。	
			3. 创建Thread类的对象，并且调用start方法开启线程。
			注意:一个线程一旦开启，就会执行run方法中的代码，run方法千万不能直接调用，直接调用run方法就相当调用了一个普通的方法而已,并没有开启新的线程。
		方式二：
			1. 自定义一个类实现Runnable接口。
			2. 实现Runnable接口的run方法，把自定义线程的任务定义在run方法上。
			3. 创建Runnable实现类对象。
			4. 创建Thread类的对象，并且把Runnable实现类的对象作为实参传递。
			5. 调用Thread对象的start方法开启一个线程。
		注意：
			1. Runnable实现类的对象并不是一个线程对象，只不过是实现了Runnable接口的普通对象而已。只有是Thread或者是Thread的子类才是线程对象。
			2. 把Runnable实现类的对象作为实参传递给Thread对象的作用就是把Runnable实现类的对象的run方法作为线程的任务代码去执行。	
	线程的生命周期：
		五种状态：创建、可运行、运行、临时阻塞、死亡
		运行状态可以通过wait()或者sleep()方法进入临时阻塞状态，进入临时阻塞状态的进程没有cpu等待资格。
		通过sleep()方式进入阻塞状态的进程一旦超过了设定的时间，可重新进入可运行状态；而通过wait()进入临时阻塞状态的进程必须通过其他线程的唤醒才可再次进入可运行状态。
	线程常用的方法：
		 Thread(String name)     初始化线程的名字
		 setName(String name)    设置线程对象名
		 getName()               返回线程的名字
		 sleep(long time)                 线程睡眠指定的毫秒数。 静态的方法， 哪个线程执行了sleep方法代码那么就是哪个线程睡眠。
		 currentThread()         返回当前的线程对象，该方法是一个静态的方法， 注意： 那个线程执行了currentThread()代码就返回那个线程的对象。
		 getPriority()           返回当前线程对象的优先级   默认线程的优先级是5
		 setPriority(int newPriority) 设置线程的优先级    虽然设置了线程的优先级，但是具体的实现取决于底层的操作系统的实现（最大的优先级是10 ，最小的1 ， 默认是5）。
	线程安全问题：
		什么时候会出现线程安全问题？
			1、有多线程存在，而且多个线程之间存在共享资源。
			2、有多个语句操作了共享语句。
		解决方案：sun提供了线程同步机制让我们解决这类问题的。
			方式一：同步代码块
				synchronized(锁对象){
					需要被同步的代码...
				}
				锁对象：任意一个对象都可以作为锁对象，因为任意一个对象内部都维护着一个状态，java的同步机制就是利用这个状态来作为锁的标识。
				同步代码块要注意事项：
					1. 任意的一个对象都可以做为锁对象。
					2. 在同步代码块中调用了sleep方法并不是释放锁对象的。
					3. 只有真正存在线程安全问题的时候才使用同步代码块,否则会降低效率的。
					4. 多线程操作的锁对象必须是唯一共享的,否则无效。
			方式二：同步函数
				synchronized function(){
					
				}
				同步函数要注意的事项 ：
					1. 同步函数就是使用synchronized修饰一个函数。
					2. 一个非静态的同步函数的锁对象是this对象，一个静态的同步函数的锁对象是当前函数所属的类的字节码文件（class对象）。
					3. 同步函数的锁对象是固定的，不能由程序来指定。
			推荐使用： 同步代码块。
					1. 同步代码块的锁对象可以由我们随意指定，方便控制。同步函数的锁对象是固定的，不能自由指定。
					2. 同步代码块可以很方便控制需要被同步代码的范围，同步函数必须是整个函数的所有代码都被同步了。
		java中同步机制解决了线程安全问题，但是也同时引发死锁现象。
			死锁现象出现 的根本原因：
				1. 存在两个或者两个以上的线程。
				2. 存在两个或者两个以上的共享资源。
			解决方案： 没有方案。只能尽量避免发生而已。
	线程通讯：
		当一个线程完成自己的任务时通知另外线程完成另一个任务。
			wait():    等待   如果线程调用了wait()方法，该线程就会立即进入一个以锁对象为标识符的线程池中等待。并立即释放加在该线程上的锁。
			notify()： 唤醒   如果线程调用了notify()方法，会立即唤醒以锁对象为标识符的线程池中的其中一个等待线程
			notifyAll() : 唤醒线程池所有等待线程。
		wait与notify方法要注意的事项：
			1. wait方法与notify方法是属于Object对象的。
			2. wait方法与notify方法必须要在同步代码块或者是同步函数中才能使用。
			3. wait方法与notify方法必需要由锁对象调用。
		线程的终止：
		 	1. 停止一个线程我们一般都会通过一个变量去控制的。
			2. 如果需要停止一个处于等待状态下的线程，那么我们需要通过变量配合notify()方法或者interrupt()来使用。
			notify唤醒线程是随机的，无法指定具体唤醒哪一个，而且必须存在于同步代码块中，但是唤醒方式相对缓和，被唤醒线程不会受伤。
			interrupt唤醒线程相对粗暴，被唤醒线程会接受到一个异常，但它可以精确指定唤醒哪个线程，并且对同步代码块无要求。
	守护线程：
		在一个进程中如果只剩下了守护线程，那么守护线程也会死亡。
			d.setDaemon(boolean flag); 设置线程是否为守护线程，true为守护线程， false为非守护线程。
			d.isDaemon()); 判断线程是否为守护线程。
	线程让步join
		一个线程如果执行join语句，那么就有新的线程加入，执行该语句的线程必须要让步给新加入的线程先完成任务，然后才能继续执行。
		
		

正则表达式
	正则表达式是用了特殊字符来操作字符串的一个规则。
	预定义字符类：
		.  任意字符
		\d 数字字符[0-9]
		\D 非数字字符[^0-9]
		\s 空白字符[\t\n\x0B\f\r]
		\S 非空白字符[^\s]
		\w 单词字符[0-9a-zA-Z_]
		\W 非单词字符[^\w]
	数量词
		X? 零次或者一次
		X* 零次或者多次
		X+ 一次或者多次
		X{n} 恰好n次
		X{n,} 至少n次
		X{n,m} 至少n次，至多m次
	范围词
		[abc] 指定范围中的一个字符
		[^abc] 指定范围外的一个字符
		[a-v] a-v之间的一个字符
		[a-c[m-p]] 指定两个范围的并集中的一个字符
		[a-f&&[f-h]]  指定两个范围的交集中的一个字符
	边界匹配
		^ 行的开头 
		$ 行的结尾 
		\b 单词边界 
		\B 非单词边界 
		\A 输入的开头 
		\G 上一个匹配的结尾 
		\Z 输入的结尾，仅用于最后的结束符（如果有的话） 
		\z 输入的结尾 
	特殊
		(com|cn|net) 字符串
		( )   分组 为了让正则内容可以复用
		\2    引用前面的组，组号从1开始;在正则规则外引用的话用$组号方式
	正则的几种应用：
		匹配：matches()
		切割：split()
		替换：replaceAll()
		查找：使用正则对象
			1、Pattern
			2、Matcher
			使用方式
				先把正则字符串编译成正则对象 Pattern p = Pattern.compile(reg)
				使用正则对象匹配字符串产生一个匹配器对象 Matcher m = p.matcher(content)
				使用循环拿到所有符合规则的串
				while(m.find()){
					m.group()
				}
			典型应用：
				Pattern p = Pattern.compile("\\b[a-z]{3}\\b");
				Matcher m = p.matcher(content);
				while(m.find()){
					System.out.println(m.group());
				}