JavaMail
	 JavaMail是由Sun定义的一套收发电子邮件的API，但它并没有包含在JDK中，而是作为JavaEE的一部分。



协议
	JavaMail服务程序可以有选择地实现某些邮件协议，常见的邮件协议包括：
		SMTP：简单邮件传输协议，用于发送电子邮件的传输协议，默认端口25
		POP3：用于接收电子邮件的标准协议，默认端口110
		IMAP：互联网消息协议，是POP3的替代协议。
	这三种协议都有对应SSL加密传输的协议，分别是SMTPS，POP3S和IMAPS。



API
	javax.mail.Session：上下文环境信息，如服务器的主机名、端口号、协议名称等  
	javax.mail.Message：邮件模型，发送邮件和接收邮件的媒介，封装了邮件的信息，如发件人、收件人、邮件标题、邮件内容等  
	javax.mail.Transport：连接邮件SMTP服务器，发送邮件  
	javax.mail.Store：连接邮件POP3、IMAP服务器，收取邮件



简单步骤
	1、创建Properties 类用于记录邮箱的一些属性
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.163.com");
        props.setProperty("mail.smtp.auth", "true");
    2、构建授权信息，用于进行SMTP进行身份验证
        Authenticator authenticator = new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("用户名", "密码");
            }
        };
    3、使用环境属性和授权信息，创建邮件会话
    	Session mailSession = Session.getInstance(props, authenticator);
    4、创建邮件消息
    	MimeMessage message = new MimeMessage(mailSession);
    	message.setFrom(new InternetAddress("bytingyu@163.com"));// 设置发件人
    	message.setRecipient(RecipientType.TO, new InternetAddress("bytingyu@163.com")); // 设置收件人的邮箱
		message.setSubject("测试邮件");// 设置邮件标题
		message.setContent("这是一封测试邮件", "text/html;charset=UTF-8");// 设置邮件的内容体
	5、发送邮件
		Transport.send(message);