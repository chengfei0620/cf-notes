Shiro
	Apache Shiro 是一个安全框架
Shiro功能点
	Authentication: 身份认证/登录，验证用户是不是拥有相应的身份
	Authorization:  授权，即权限验证，验证某个已认证的用户是否拥有某个权限
	Session Manager:会话管理，即用户登录后就是一次会话，在没有退出之前，它的所有 信息都在会话中
	Cryptography:   加密，保护数据的安全性
	Caching:        缓存，比如用户登录后，其用户信息、拥有的角色/权限不必每次去查
	Concurrency:    Shiro支持多线程应用的并发验证
	Run As:         允许一个用户假装为另一个用户（如果他们允许）的身份进行访问；
	Remember Me:    记住我，这个是非常常见的功能，即一次登录后，下次再来的话不用登录了
主要对象
	Subject:应用代码直接交互的对象是Subject，这是Shiro的门面,代表着当前访问对象
	SecurityManager:安全管理器,这是Shiro的核心,负责调度Shiro中的各个组件
	Realm:它负责向SecurityManager传递收集起来的权限
	Authenticator:负责Subject认证
	Authorizer：授权器、即访问控制器，用来决定主体是否有权限进行相应的操作
过滤器
	anon:匿名可用                            /admin/**=anon
	authc:登录认证可用                       /admin/**=authc
	authcBasic:HttpBasic认证可用             /admin/**=authcBasic
	logout  注销                              
	port    端口                             /admin/**=port[8081]       
	rest   请求方式                          /admin/**=perms[user:method]
	roles   角色                             /admin/**=["admin","guset"]
认证
	Shiro的身份验证需要向Shiro提供principals和credentials,基本流程是:
		1) 收集用户身份/凭证
		2) 调用Subject.login()进行登录认证,认证失败会返回响应的异常
		3) 自定义Realm类集成AuthorizingRealm类,实现doGetAuthenticationInfo()方法自定义认证逻辑
	多个Realm 进行验证，验证规则通过AuthenticationStrategy接口指定
		FirstSuccessfulStrategy:只要有一个Realm验证成功即可，只返回第一个Realm身份验证成功的认证信息
		AtLeastOneSuccessfulStrategy(默认):只要有一个Realm验证成功即可，将返回所有Realm身份验证成功的认证信息
		AllSuccessfulStrategy:所有Realm验证成功才算成功，且返回所有 Realm身份验证成功的认证信息
授权
	授权,在应用中控制谁访问哪些资源.在授权中需了解的几个关键对象:Subject、Resource、Permission、Role
		主体(Subject):访问应用的用户,在Shiro中使用Subject代表
		资源(Resource):在应用中用户可以访问的URL
		权限(Permission):安全策略中的原子授权单位,通过权限我们可以表示在应用中用户有没有操作某个资源的权力
		角色(Role):权限的集合，一般情况下会赋予用户角色而不是权限
	授权方式
		编程方式
			subject.hasRole("roleName")        基于角色
			subject.isPermitted("user:create") 基于权限
		注解式
			@RequiresAuthentication:当前Subject已经通过login进行了身份验证
			@RequiresUser:当前Subject已经身份验证或者通过记住我登录的
			@RequiresGuest:表示当前Subject没有身份验证,即是游客身份
			@RequiresRoles(value={"admin","user"},logical=Logical.AND):当前Subject需要角色admin和user
			@RequiresPermissions(value={"user:a","user:b"},logical= Logical.OR):表示当前Subject需要权限user:a或user:b
		标签式
			guest:用户没有身份验证时显示相应信息,即游客访问信息
			user :用户已经经过认证/记住我登录后显示相应的信息
			authenticated:用户已经身份验证通过,即Subject.login登录成功,不是记住我登录的
			notAuthenticated:用户未进行身份验证,即没有调用Subject.login进行登录,但是记住我登录的属于该类	
			pincipal:显示用户身份信息,默认调用Subject.getPrincipal()获取,即Primary Principal
			hasRole:如果当前Subject有角色将显示内容
			lacksRole:如果当前Subject没有角色将显示内容
			hasAnyRoles:拥有指定权限集中的一个时,显示内容
			hasPermission:如果当前Subject有权限将显示内容
			lacksPermission:如果当前Subject没有权限将显示内容
	权限规则
		资源操作符:操作:对象实例ID

	
	
	
	
	
	
	
	