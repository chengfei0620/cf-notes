Spring Security
	
	配置表单
        httpSecurity
                .formLogin().loginPage("/login.html")//登录表单页面URL
                .loginProcessingUrl("/user/login") //登录方法URL
                .successHandler(null) //登录成功逻辑
                .failureHandler(null) //登录失败逻辑
                .and()
                .authorizeRequests()//指定下面的东西需要授权
                .antMatchers("/login.html").permitAll()//放行指定的url
                .anyRequest().authenticated()//任何未指定的请求都要经过授权
                .and()
                .csrf().disable()//禁用csrf跨站防护
				
				
SpringSecurity是一个过滤器链
	SecurityContextPersistenceFilter     维持session和本地线程中认证信息的唯一
	UsernamePasswordAuthenticationFilter 用户名密码认证
	BasicAuthenticationFilter            基础认证
	FilterSecuityInterceptor             最后一个环节,直接决定身份认证是否成功
	ExecptionTranslationFilter           用于捕获异常,处理
	

	
自定义用户认证逻辑
	用户信息的获取封装在UserDetailService接口中
		UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException;
		UserDetails 封装了用户登录后返回的信息
		    Collection<? extends GrantedAuthority> getAuthorities();  权限
			String getPassword();  密码
			String getUsername();  用户名
			boolean isAccountNonExpired();  账号没过期
			boolean isAccountNonLocked();   账号没锁定
			boolean isCredentialsNonExpired();  密码没过期
			boolean isEnabled();  账号已激活
	密码的加密解密
		public interface PasswordEncoder {
			String encode(CharSequence var1);  //账号加密
			boolean matches(CharSequence var1, String var2); //账号匹配
		}
		向容器内注入PasswordEncoder实例后,容器在账户登录时会自动调用matches方法匹配加密密码
			@Bean
			public PasswordEncoder passwordEncoder(){
				return new BCryptPasswordEncoder();
			}
	登录成功\失败处理
		成功失败的默认处理是这样的
			成功跳转到原来的访问页面
			失败跳转到错误页面
		如果想自定义成功的行为,需要实现AuthenticationSuccessHandler的onAuthenticationSuccess的方法
		如果想自定义失败的行为,需要实现AuthenticationFailureHandler的onAuthenticationFailure的方法
	获取用户认证信息
		SecurityCOntextHolder.getContext().getAuthentication()
		@AuthenticationPrincipal UserDetails user           只获取UserDetails对象
		
