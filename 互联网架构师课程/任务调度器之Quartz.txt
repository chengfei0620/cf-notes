Quartz
	这是一个开源的作业调度框架,通过Quartz可以快速完成任务调度的工作.



关键API
	Job:
		这是一个接口，只有一个方法execute(JobExecutionContext context),开发者实现该接口定义运行任务.
	JobExecutionContext:
		这个类提供了调度上下文的各种信息,Job运行时的信息保存在JobDataMap实例中.
	JobDetail：
		Quartz在每次执行Job时,都重新创建一个Job实例,所以它不直接接受一个Job的实例,而是接收一个Job实现类,运行时通过反射机制实例化Job。因此需要通过一个类来描述Job的实现类及其它相关的静态信息,如Job名字、描述、关联监听器等信息，JobDetail承担了这一角色。
	Trigger:
		用来描述Job执行的时间触发规则。主要有SimpleTrigger和CronTrigger这两个子类。
			当仅需触发一次或者以固定时间间隔周期执行，SimpleTrigger是最适合的选择；
			而CronTrigger则可以通过Cron表达式定义出各种复杂时间规则的调度方案。
	Calendar:
		它是一些日历特定时间点的集合,一个Trigger可以和多个Calendar关联，以便排除或包含某些时间点。
	Scheduler：
		代表一个Quartz的独立运行容器，Trigger和JobDetail可以注册到Scheduler中，两者在Scheduler中拥有各自的组及名称，组及名称是Scheduler查找定位容器中某一对象的依据。Scheduler定义了多个接口方法，允许外部通过组及名称访问和控制容器中Trigger和JobDetail。
	ThreadPool:
		Scheduler使用一个线程池作为任务运行的基础设施，任务通过共享线程池中的线程提高运行效率。



Quartz触发器Trigger
	Trigger定义了Quartz工作的时间，主要有下面的实现类
		SimpleTrigger:可以设置开始时间、结束时间和重复间隔
	        SimpleTrigger trigger = (SimpleTrigger)TriggerBuilder.newTrigger().withIdentity("simple")
	            .startAt(new Date())
	            .endAt(new Date())
	            .withSchedule(
	                    SimpleScheduleBuilder.simpleSchedule()
	                    .withRepeatCount(3)
	                    .withIntervalInSeconds(2)
	        	).build();
		CronTrigger:使用Unix cron表达式定义任务执行时间
			Cron表达式
				*   *   *   *   *   *   *  
				秒  分  时  日  月  周  年  
			可以使用cron在线生成器生成
			CronTrigger trigger = (CronTrigger) TriggerBuilder.newTrigger().withIdentity("cron")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule("0/3 * * * * ?")
                ).build();



Quartz调度器Scheduler
	SchedulerFactory    -- 调度程序工厂
		StdSchedulerFactory   -- Quartz默认的SchedulerFactory
		DirectSchedulerFactory  -- SchedulerFactory的直接实现,通过它可以直接构建Scheduler、threadpool 等
	用法
	    Scheduler scheduler = new StdSchedulerFactory().getScheduler();
	    scheduler.start();
	    scheduler.scheduleJob(jobDetail,trigger);
	API
		void start()  启动计划
		void standby() 挂起计划
		void shutdown(boolean flag) 关闭计划
			参数用来指定是否等待所有任务完成再计划关闭
		Date scheduleJob(jobDetail,trigger) 绑定计划任务和计划周期，返回任务列表首次执行时间


Spring整合Quartz
		<!-- 工作的bean -->
    <bean id="myJob" class="com.haoxin.web.ops.ad.util.QuartzJob" />
	
    <!-- job的配置开始 -->
    <bean id="myJobDetail"
        class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject">
            <ref bean="myJob" />
        </property>
        <property name="targetMethod">
            <value>work</value>
        </property>
    </bean>
    <!-- job的配置结束 -->


    <!-- 调度的配置开始 -->
    <bean id="crontestJobTrigger" class="org.springframework.scheduling.quartz.CronTriggerFactoryBean">
        <property name="jobDetail">
            <ref bean="myJobDetail" />
        </property>
        <property name="cronExpression">
            <value>10 0/1 * * * ?</value>  <!-- Cron表达式“10 */1 * * * ?”意为：从10秒开始，每1分钟执行一次。  -->
        </property>
    </bean>
    <!-- 调度的配置结束 -->


    <!-- 启动触发器的配置开始 -->
    <bean name="startQuertz" lazy-init="false" autowire="no"
        class="org.springframework.scheduling.quartz.SchedulerFactoryBean">
        <property name="triggers">
            <list>
                <ref bean="crontestJobTrigger" />

            </list>
        </property>
    </bean>
    <!-- 启动触发器的配置结束 -->